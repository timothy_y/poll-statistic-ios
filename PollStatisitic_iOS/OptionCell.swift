//
//  OptionCell.swift
//  PollStatisitic_iOS
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class OptionCell: UITableViewCell {

    var optionWidth: Double = 0.0
    var optionName: String = ""
    
    
    @IBOutlet weak var optionLevel: OptionLevel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setProporties(){
        optionLevel.optionWidth = self.optionWidth * Double(optionLevel.frame.width)
        optionLevel.optionName = self.optionName
        optionLevel.tintColor = UIColor.darkGray
        optionLevel.backgroundColor = UIColor.darkGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


}
