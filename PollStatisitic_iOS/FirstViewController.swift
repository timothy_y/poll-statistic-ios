
//
//  FirstViewController.swift
//  PollStatisitic_iOS
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData



class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    //MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    var client: MSClient?
    var refreshControl: UIRefreshControl!
    
    var polls: [Poll] = [Poll]()
    var voted: [String] = [String]()
    
    func refresh(sender: AnyObject){
        
        getData()
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Along with auto layout, these are the keys for enabling variable cell height
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func getData(){
        
        self.client = MSClient(applicationURLString: "https://pollstatistic.azurewebsites.net")
        let table = self.client?.table(withName: "Polls")
            
        table?.read { (result, error) in
            if let err = error {
                print("ERROR ", err)
            } else if let items = result?.items {
                for item in items {
                    var options: [String] = (item["pollOptions"] as! String).components(separatedBy: "\n")
                    options.removeLast()
                    
                    let title: String = item["pollTitle"] as! String
                    
                    var rates: [Int] = [Int]()
                    if(self.voted.contains(item["id"] as! String)){
                    
                        var ratesStrings: [String]  = (item["optionsRate"] as! String).components(separatedBy: "\n")
                        ratesStrings.removeLast()
                        
                        for str in ratesStrings {
                            let num: Int  = Int(str)!
                            rates += [num]
                        }
                    }
                    let rate: Int = (item["pollRate"] as? Int)!
                    self.polls += [Poll(title: title, options: options, rates: rates, pollRate: rate) ]
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.polls.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(true){
            let cell: VoteCell = self.tableView.dequeueReusableCell(withIdentifier: "voteCell") as! VoteCell
            //cell.vote.titleLabel?.text = self.polls[indexPath.row].title
            
            return cell
        }else{
        
            let cell: PollCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! PollCell
            cell.titleLabel.text = self.polls[indexPath.row].title
            cell.options = self.polls[indexPath.row].options
            cell.rates = self.polls[indexPath.row].rates
            cell.pollRate = self.polls[indexPath.row].pollRate
        
            return cell
        }
    }
    
    
}

