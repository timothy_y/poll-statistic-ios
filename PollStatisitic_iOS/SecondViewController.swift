
//
//  SecondViewController.swift
//  PollStatisitic_iOS
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController {

    //MARK - Outlets
    @IBOutlet weak var AddButton: UIButton!
    @IBOutlet weak var RemoveButton: UIButton!
    @IBOutlet weak var PostButton: UIButton!
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var option1Field: UITextField!
    @IBOutlet weak var option2Field: UITextField!
    
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var settingsView: UIView!
    
    @IBOutlet weak var anonSwitcher: UISwitch!
    @IBOutlet weak var revoteSwitcher: UISwitch!
    
    @IBOutlet weak var scroolView: UIScrollView!
    //MARK - Properties
    var optionFields: [UITextField] = [UITextField]()
    var client: MSClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        optionFields += [option1Field, option2Field]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func addClick(_ sender: Any) {
        
        let newOptionField: UITextField = UITextField(frame: CGRect(x: optionFields.last!.frame.origin.x,
                                                                    y: optionFields.last!.frame.origin.y + 50,
                                                                    width: optionFields.last!.frame.size.width,
                                                                    height: optionFields.last!.frame.size.height ))
        newOptionField.backgroundColor = UIColor.lightGray
        newOptionField.borderStyle = .roundedRect
        newOptionField.alpha = 0.5
        optionsView.addSubview(newOptionField)
        optionFields += [newOptionField]
        
        optionsView.frame.size = CGSize(width: optionsView.frame.size.width, height: optionsView.frame.size.height + 50)
        
        settingsView.frame.origin = CGPoint(x: settingsView.frame.origin.x , y: settingsView.frame.origin.y + 50 )
        
        scroolView.frame.size = CGSize(width: scroolView.frame.size.width, height: scroolView.frame.size.height + 50)
        
    }
    @IBAction func removeClick(_ sender: Any) {
        if(optionFields.count > 2){
            optionFields.last?.removeFromSuperview()
            optionFields.removeLast()
        
            optionsView.frame.size = CGSize(width: optionsView.frame.size.width, height: optionsView.frame.size.height - 50)
        
            settingsView.frame.origin = CGPoint(x: settingsView.frame.origin.x , y: settingsView.frame.origin.y - 50 )
        
            scroolView.frame.size = CGSize(width: scroolView.frame.size.width, height: scroolView.frame.size.height - 50)
        }

    }
   
    @IBAction func postClick(_ sender: Any) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.client = MSClient(applicationURLString: "https://pollstatistic.azurewebsites.net")
        let table = self.client?.table(withName: "Polls")
        
        var options: String = String()
        for optionField in optionFields {
                options += optionField.text! + "\n"
        }
        
        
        let newItem = ["id": "custom-id", "pollTitle": titleField.text,"pollOptions": options, "isAnonymous": anonSwitcher.isOn, "isRevoteOn": revoteSwitcher.isOn ] as [String : Any]
        table?.insert(newItem) { (result, error) in
            if let err = error {
                print("ERROR ", err)
            } else if let item = result {
                
            }
            LoadingOverlay.shared.hideOverlayView()
        }
    }


}

