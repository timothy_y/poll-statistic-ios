//
//  Poll.swift
//  Blank App
//
//  Created by Admin on 26.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

class Poll {
    var title: String
    var options: [String]
    var rates: [Int]
    var pollRate: Int
    
    init(title name: String, options pollOptions: [String], rates pollRates: [Int], pollRate rate: Int ){
        title = name
        options = pollOptions
        rates = pollRates
        pollRate = rate
    }
}
