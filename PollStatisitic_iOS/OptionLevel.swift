//
//  OptionLevel.swift
//  PollStatisitic_iOS
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class OptionLevel: UIView {
    var optionWidth: Double = 0.0
    var optionName: String = ""
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(UIColor.green.cgColor)
        context?.setLineWidth(3.0)
        context?.addRect(CGRect(x: 0 , y: 0, width: optionWidth, height: 25.0))
        
        context?.setStrokeColor(UIColor.green.cgColor)
        context?.fillPath()
        
        let textFontAttributes = [
            NSFontAttributeName: UIFont(name: "Georgia", size: 18)!,
            NSForegroundColorAttributeName: UIColor.black,
            ] as [String : Any]
        
        (optionName as NSString).draw(at: CGPoint(x: 5, y: 2), withAttributes: textFontAttributes)
        
    }

}
