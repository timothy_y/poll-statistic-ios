//
//  PollCell.swift
//  Blank App
//
//  Created by Admin on 26.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class PollCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource  {

  
   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionsTable: UITableView!
    
    var options: [String] = [String]()
    var rates: [Int] = [Int]()
    var pollRate: Int = 1
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        // Do any additional setup after loading the view, typically from a nib.
        optionsTable.delegate = self
        optionsTable.dataSource = self
        
        // Along with auto layout, these are the keys for enabling variable cell height
        
        self.optionsTable.estimatedRowHeight = 80
        self.optionsTable.rowHeight = UITableViewAutomaticDimension
        
        optionsTable.isScrollEnabled = false;
        
        let height: Int  = options.count * 25
        optionsTable.frame = CGRect(x: optionsTable.frame.origin.x, y: optionsTable.frame.origin.y,
                                    width: optionsTable.frame.size.width, height: CGFloat(height));
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: OptionCell = self.optionsTable.dequeueReusableCell(withIdentifier: "cell") as! OptionCell
        cell.optionName = self.options[indexPath.row]
        cell.optionWidth = Double(self.rates[indexPath.row]) / Double(self.pollRate)
        cell.setProporties()
        return cell
    }
    
}
